use spfl::{CompressedRowMatrix, SparseMatrix};
use std::sync::{
    atomic::{AtomicBool, Ordering},
    Arc,
};
use textplots::{Chart, Plot, Shape};

fn main() {
    // First, we'll decide how fine a discretization to use in space.
    let n = 32;
    let dx = 1.0 / (n as f64);

    // Next, we'll fill some vectors describing what entries `(i, j)` of the
    // matrix are non-zero, as well as the values.
    let mut entries = vec![];
    let mut values = vec![];

    for i in 0..n {
        entries.push((i, i));
        values.push(-2.0 / dx);

        entries.push((i, (i + 1) % n));
        values.push(1.0 / dx);

        entries.push(((i + 1) % n, i));
        values.push(1.0 / dx);
    }

    // Then we'll pack these into a sparse matrix data structure.
    let a = CompressedRowMatrix::new_from_triplet((n, n), &entries, &values);

    // The initial profile will be a parabolic hump around the point `0.5`.
    let mut u = ndarray::Array1::<f64>::zeros(n);
    let y = 0.5;
    let r = 0.25;
    for i in 0..n {
        let x = (i as f64) / (n as f64);
        u[i] = (0.0 as f64).max(1.0 - f64::powi((x - y) / r, 2));
    }

    let mut us = vec![];
    us.push(u.clone());

    // We'll also need some scratch vectors.
    let mut du_dt = ndarray::Array1::<f64>::zeros(n);
    let mut v = ndarray::Array1::<f64>::zeros(n);

    // Finally, we need to decide how long to integrate the system for. We'll
    // go for 4π revolutions.
    let final_time = 4.0 * std::f64::consts::PI;
    let dt = 0.5 / (n as f64);
    let num_steps = (final_time / dt).ceil() as usize;

    // Now the good part! Loop through for every time step and solve the wave
    // equation using a leapfrog scheme:
    //
    //     u += 0.5 * dt * du_dt
    //     du_dt += dt * A * u
    //     u += 0.5 * dt * du_dt
    for _ in 0..num_steps {
        u = &u + 0.5 * dt * &du_dt;
        v.fill(0.0);
        a.mult(u.as_slice().unwrap(), v.as_slice_mut().unwrap());
        du_dt = &du_dt + dt * &v;
        u = &u + 0.5 * dt * &du_dt;
        us.push(u.clone());
    }

    // Finally, we'll use the package `textplots` to visualize the solution in
    // the terminal. First, we'll get a reference to the current terminal and
    // turn off the cursor. We'll then set an interrupt handler whenever the
    // user hits Ctrl+C so that the cursor gets turned back on when we exit.
    let term = console::Term::stdout();
    term.hide_cursor().unwrap();
    term.clear_screen().unwrap();

    let should_run = Arc::new(AtomicBool::new(true));
    let should_run_ctrlc_ref = should_run.clone();

    ctrlc::set_handler(move || {
        should_run_ctrlc_ref
            .as_ref()
            .store(false, Ordering::Relaxed);
    })
    .unwrap();

    // Now we need to make a vector storing pairs `(x, u(x))`.
    let mut points = vec![];
    for i in 0..n {
        let x = ((i as f64) / n as f64) as f32;
        points.push((x, us[0][i] as f32));
    }

    // For each time step,
    let mut t = 0;
    while should_run.as_ref().load(Ordering::Acquire) {
        // reset the terminal cursor to 0,
        term.move_cursor_to(0, 0).unwrap();

        // plot the current value of the solution,
        Chart::new_with_y_range(120, 60, 0.0, 1.0, 0.0, 1.0)
            .lineplot(&Shape::Lines(&points))
            .display();

        // update the `points` array for the next value of the solution,
        let u = &us[t];
        for i in 0..n {
            let x = ((i as f64) / n as f64) as f32;
            points[i] = (x, u[i] as f32);
        }

        // increment the timestep, and sleep for 10 ms.
        t = (t + 1) % num_steps;
        std::thread::sleep(std::time::Duration::from_millis(10));
    }

    // Turn the cursor back on
    let term = console::Term::stdout();
    term.show_cursor().unwrap();
}
