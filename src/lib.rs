#![warn(missing_docs)]
/*!

`spfl` is a sparse matrix factorization library.

*/

mod matrix;
mod graph;

pub use graph::Graph;
pub use matrix::{
    CompressedColMatrix, CompressedRowMatrix, SparseMatrix, SparsityPattern,
};
