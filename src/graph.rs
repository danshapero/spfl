/// Data type describing directed graphs, i.e. collections of edges or arrows
/// between finite source and target sets. Used to describe the elimination
/// graph for sparse matrix factorzation algorithms.
pub struct Graph {
    shape: (usize, usize),
    data: Vec<Vec<usize>>,
}

impl Graph {
    /// Create an empty graph with a given number of source and target
    /// vertices
    pub fn new(shape: (usize, usize)) -> Self {
        let empty = Vec::new();
        let data = vec![empty; shape.0];
        Self { shape, data }
    }

    /// Return the number of source and target vertices
    pub fn shape(&self) -> (usize, usize) {
        self.shape
    }

    /// Return a slice of the target vertices `j` such that the graph contains
    /// the edge `i, j`
    pub fn neighbors(&self, i: usize) -> &[usize] {
        assert!(i < self.shape.0);
        &self.data[i]
    }

    /// Return the number of edges that have `i` as source vertex
    pub fn degree(&self, i: usize) -> usize {
        assert!(i < self.shape.0);
        self.data[i].len()
    }

    /// Return `true` if the graph contains the edge `i, j`, `false` otherwise
    pub fn contains(&self, i: usize, j: usize) -> bool {
        assert!(i < self.shape.0);
        assert!(j < self.shape.1);

        for &k in self.neighbors(i) {
            if k == j {
                return true;
            }
        }

        false
    }

    /// Add the edge `i, j` to the graph; do nothing if it's already there
    pub fn push(&mut self, i: usize, j: usize) {
        if !self.contains(i, j) {
            self.data[i].push(j);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_graphs() {
        let n = 16;
        let mut graph = Graph::new((n, n));
        assert_eq!(graph.shape(), (n, n));
        for i in 0..n {
            assert_eq!(graph.degree(i), 0);

            for j in 0..n {
                assert!(!graph.contains(i, j));
            }
        }

        for i in 0..n {
            let j = (i + 1) % n;
            graph.push(i, j);
        }

        for i in 0..n {
            let j = (i + 1) % n;
            assert_eq!(graph.degree(i), 1);
            assert!(graph.contains(i, j));
            assert!(!graph.contains(j, i));

            let js = graph.neighbors(i);
            assert_eq!(js.len(), 1);
            assert_eq!(js[0], j);
        }
    }
}
