use num_traits::Num;
use std::iter::zip;
use std::marker::PhantomData;
use std::ops::AddAssign;

/// Some stuff to deal with matrix ordering
pub struct RowOrder;
pub struct ColOrder;

pub trait MatrixOrder {}
impl MatrixOrder for RowOrder {}
impl MatrixOrder for ColOrder {}

/// Trait describing sparse matrices, linear operators that (in a certain
/// choice of basis) have mostly zero entries.
pub trait SparseMatrix<T> {
    /// Create a new sparse matrix from a list of the positions `(i, j)` of
    /// non-zero entries and values `a_ij`
    fn new_from_triplet(
        shape: (usize, usize),
        nonzeros: &[(usize, usize)],
        values: &[T],
    ) -> Self;

    /// Return the dimension of the domain and range spaces of the operator
    fn shape(&self) -> (usize, usize);

    /// Get an optional reference to a non-zero entry of the matrix
    fn get(&self, i: usize, j: usize) -> Option<&T>;

    /// Get an optional mutable reference to a non-zero entry of the matrix
    fn get_mut(&mut self, i: usize, j: usize) -> Option<&mut T>;

    /// Multiply the matrix by a vector and store the result in another vector
    fn mult(&self, x: &[T], y: &mut [T]);

    /// Multiply the transpose of the matrix by a vector and store the result
    /// in another vector
    fn mult_transpose(&self, x: &[T], y: &mut [T]);
}

/// Describes the positions of the non-zero entries in a sparse matrix.
pub struct SparsityPattern {
    shape: (usize, usize),
    offsets: Vec<usize>,
    entries: Vec<usize>,
}

impl SparsityPattern {
    /// Create a new sparsity pattern from a list of the positions `(i, j)` of
    /// non-zero entries
    pub fn new_from_entry_list(
        shape: (usize, usize),
        nonzeros: &[(usize, usize)],
    ) -> Self {
        for &(i, j) in nonzeros {
            assert!(i < shape.0 && j < shape.1);
        }

        let mut offsets = vec![0; shape.0 + 1];
        let mut entries = vec![0; nonzeros.len()];

        // Count the number of times that `i` appears in the non-zero entries
        let mut counts = vec![0; shape.0];
        for &(i, _) in nonzeros {
            counts[i] += 1;
        }

        // The `offsets` array is the cumulative sum of the non-zero counts
        for i in 0..shape.0 {
            offsets[i + 1] += offsets[i] + counts[i];
        }

        // Fill the entries array
        let mut counts = vec![0; shape.0];
        for &(i, j) in nonzeros {
            let index = offsets[i] + counts[i];
            entries[index] = j;
            counts[i] += 1;
        }

        Self {
            shape,
            offsets,
            entries,
        }
    }

    /// Return the index in the entries array of `(i, j)` if it is in the
    /// sparsity pattern
    pub fn index(&self, i: usize, j: usize) -> Option<usize> {
        for index in self.offsets[i]..self.offsets[i + 1] {
            let k = self.entries[index];
            if k == j {
                return Some(index);
            }
        }

        None
    }

    /// Return the number of rows & columns of the sparsity pattern
    pub fn shape(&self) -> (usize, usize) {
        self.shape
    }

    /// Return the total number of nonzeros in the sparsity pattern
    pub fn num_nonzero(&self) -> usize {
        self.entries.len()
    }

    /// Return the number of nonzeros that `i` points to
    pub fn nonzeros(&self, i: usize) -> &[usize] {
        &self.entries[self.offsets[i]..self.offsets[i + 1]]
    }

    /// Return the offsets array
    pub fn offsets(&self) -> &[usize] {
        &self.offsets
    }

    /// Return the array of target nodes
    pub fn entries(&self) -> &[usize] {
        &self.entries
    }
}

/// Data structure for sparse matrices, i.e. matrices whose entries are mostly
/// zeros.
pub struct CompressedSparseMatrix<T: Num + Copy + AddAssign, Order: MatrixOrder>
{
    pattern: SparsityPattern,
    values: Vec<T>,
    order: PhantomData<Order>,
}

/// This kernel does `A * x` when `A` is a compressed row matrix and
/// `transpose(A) * x` when `A` is a column matrix
fn csr_mult<T: Num + Copy + AddAssign, Order: MatrixOrder>(
    a: &CompressedSparseMatrix<T, Order>,
    x: &[T],
    y: &mut [T],
) {
    let offsets = &a.pattern.offsets;
    let entries = &a.pattern.entries;
    let values = &a.values;

    for i in 0..y.len() {
        let mut z = T::zero();

        for (&j, &a_ij) in
            zip(entries, values).take(offsets[i + 1]).skip(offsets[i])
        {
            z += a_ij * x[j];
        }
        y[i] += z;
    }
}

/// This kernel does `A * x` when `A` is a compressed column matrix and
/// `transpose(A) * x` when `A` is a row matrix
fn csc_mult<T: Num + Copy + AddAssign, Order: MatrixOrder>(
    a: &CompressedSparseMatrix<T, Order>,
    x: &[T],
    y: &mut [T],
) {
    let offsets = &a.pattern.offsets;
    let entries = &a.pattern.entries;
    let values = &a.values;

    for j in 0..x.len() {
        let x_j = x[j];
        for (&i, &a_ij) in
            zip(entries, values).take(offsets[j + 1]).skip(offsets[j])
        {
            y[i] += a_ij * x_j;
        }
    }
}

impl<T: Num + Copy + AddAssign, Order: MatrixOrder>
    CompressedSparseMatrix<T, Order>
{
    /// Create a zero sparse matrix with entries allocated by a pre-specified
    /// sparsity pattern
    pub fn new_from_pattern(pattern: SparsityPattern) -> Self {
        let values = vec![T::zero(); pattern.num_nonzero()];
        Self {
            pattern,
            values,
            order: PhantomData,
        }
    }
}

/// Type of compressed sparse matrices in row-major ordering
pub type CompressedRowMatrix<T> = CompressedSparseMatrix<T, RowOrder>;

/// Type of compressed sparse matrices in column-major ordering
pub type CompressedColMatrix<T> = CompressedSparseMatrix<T, ColOrder>;

impl<T: Num + Copy + AddAssign> SparseMatrix<T> for CompressedRowMatrix<T> {
    /// Create a new compressed row matrix
    fn new_from_triplet(
        shape: (usize, usize),
        nonzeros: &[(usize, usize)],
        values: &[T],
    ) -> Self {
        // FIXME this is bad
        let pattern = SparsityPattern::new_from_entry_list(shape, nonzeros);
        let mut result = CompressedRowMatrix::<T>::new_from_pattern(pattern);
        for (&(i, j), &value) in std::iter::zip(nonzeros, values) {
            let a_ij = result.get_mut(i, j).unwrap();
            *a_ij = value;
        }
        result
    }

    /// Return the number of rows and columns of the matrix
    fn shape(&self) -> (usize, usize) {
        self.pattern.shape
    }

    /// Return a reference to the entry of the matrix at row `i` and column `j`
    /// if that entry is in the pattern
    fn get(&self, i: usize, j: usize) -> Option<&T> {
        match self.pattern.index(i, j) {
            None => None,
            Some(index) => Some(&self.values[index]),
        }
    }

    /// Return a mutable reference to the entry of the matrix at row `i` and
    /// column `j` if that entry is in the pattern
    fn get_mut(&mut self, i: usize, j: usize) -> Option<&mut T> {
        match self.pattern.index(i, j) {
            None => None,
            Some(index) => Some(&mut self.values[index]),
        }
    }

    /// Multiply a vector by the sparse matrix and store the result in another
    /// vector
    fn mult(&self, x: &[T], y: &mut [T]) {
        assert_eq!(x.len(), self.shape().1);
        assert_eq!(y.len(), self.shape().0);

        csr_mult(self, x, y);
    }

    /// Multiply a vector by the transpose of a sparse matrix and store the
    /// result in another vector
    fn mult_transpose(&self, x: &[T], y: &mut [T]) {
        assert_eq!(x.len(), self.shape().0);
        assert_eq!(y.len(), self.shape().1);

        csc_mult(self, x, y);
    }
}

impl<T: Num + Copy + AddAssign> SparseMatrix<T> for CompressedColMatrix<T> {
    /// Create a new compressed column matrix
    fn new_from_triplet(
        shape: (usize, usize),
        nonzeros: &[(usize, usize)],
        values: &[T],
    ) -> Self {
        let nz = nonzeros.iter().map(|ij| (ij.1, ij.0)).collect::<Vec<_>>();
        let pattern =
            SparsityPattern::new_from_entry_list((shape.1, shape.0), &nz);
        let mut result = CompressedColMatrix::<T>::new_from_pattern(pattern);
        for (&(i, j), &value) in std::iter::zip(nonzeros, values) {
            let a_ij = result.get_mut(i, j).unwrap();
            *a_ij = value;
        }
        result
    }

    /// Return the number of rows and columns of the matrix
    fn shape(&self) -> (usize, usize) {
        (self.pattern.shape.1, self.pattern.shape.0)
    }

    /// Return a reference to the entry of the matrix at row `i` and column `j`
    /// if that entry is in the pattern
    fn get(&self, i: usize, j: usize) -> Option<&T> {
        match self.pattern.index(j, i) {
            None => None,
            Some(index) => Some(&self.values[index]),
        }
    }

    /// Return a mutable reference to the entry of the matrix at row `i` and
    /// column `j` if that entry is in the pattern
    fn get_mut(&mut self, i: usize, j: usize) -> Option<&mut T> {
        match self.pattern.index(j, i) {
            None => None,
            Some(index) => Some(&mut self.values[index]),
        }
    }

    /// Multiply a vector by the sparse matrix and store the result in another
    /// vector
    fn mult(&self, x: &[T], y: &mut [T]) {
        assert_eq!(x.len(), self.shape().1);
        assert_eq!(y.len(), self.shape().0);

        csc_mult(self, x, y);
    }

    /// Multiply a vector by the transpose of a sparse matrix and store the
    /// result in another vector
    fn mult_transpose(&self, x: &[T], y: &mut [T]) {
        assert_eq!(x.len(), self.shape().0);
        assert_eq!(y.len(), self.shape().1);

        csr_mult(self, x, y);
    }
}

impl<T: Num + Copy + AddAssign> CompressedColMatrix<T> {
    pub fn column(&self, j: usize) -> (&[usize], &[T]) {
        let offset1 = self.pattern.offsets[j];
        let offset2 = self.pattern.offsets[j + 1];
        (self.pattern.nonzeros(j), &self.values[offset1..offset2])
    }
}

impl<T: Num + Copy + AddAssign> CompressedRowMatrix<T> {
    pub fn row(&self, i: usize) -> (&[usize], &[T]) {
        let offset1 = self.pattern.offsets[i];
        let offset2 = self.pattern.offsets[i + 1];
        (self.pattern.nonzeros(i), &self.values[offset1..offset2])
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    /// Macro for testing all sparse matrix implementations
    ///
    /// Thanks to Eli Bendersky
    macro_rules! sparse_matrix_tests {
        ($($name:ident: $type:ty,)*) => {
        $(
            mod $name {
                use super::*;

                #[test]
                fn test_sparse_matrix() {
                    let length = 16;
                    let mut nonzeros = vec![];
                    let mut values = vec![];
                    for i in 0..length {
                        nonzeros.push((i, i));
                        values.push(0.0);
                        nonzeros.push((i, (i + 1) % length));
                        values.push(0.0);
                    }

                    let mut matrix = <$type>::new_from_triplet(
                        (length, length),
                        &nonzeros,
                        &values,
                    );

                    for i in 0..length {
                        let a_ii = matrix.get_mut(i, i).unwrap();
                        *a_ii = 1.0;
                        let a_ij = matrix.get_mut(i, (i + 1) % length).unwrap();
                        *a_ij = -1.0;
                        assert!(matrix.get_mut(i, (i + 2) % length).is_none());
                    }

                    for i in 0..length {
                        assert_eq!(*matrix.get(i, i).unwrap(), 1.0);
                        assert_eq!(*matrix.get(i, (i + 1) % length).unwrap(), -1.0);
                        assert!(matrix.get(i, (i + 2) % length).is_none());
                    }

                    let mut x = vec![0 as f64; length];
                    let mut y = vec![0 as f64; length];

                    for i in 0..length {
                        x[i] = 1.0;
                        y[i] = 1.0;
                    }

                    matrix.mult(&x, &mut y);
                    for i in 0..length {
                        assert_eq!(y[i], 1.0);
                    }

                    matrix.mult_transpose(&x, &mut y);
                    for i in 0..length {
                        assert_eq!(y[i], 1.0);
                    }
                }
            }
        )*
        }
    }

    sparse_matrix_tests! {
        csc: CompressedColMatrix::<f64>,
        csr: CompressedRowMatrix::<f64>,
    }

    #[test]
    fn test_creating_sparsity_pattern() {
        let length = 16;
        let mut nonzeros = vec![];
        for i in 0..length {
            nonzeros.push((i, i));
            nonzeros.push((i, (i + 1) % length));
        }

        let pattern =
            SparsityPattern::new_from_entry_list((length, length), &nonzeros);
        assert_eq!(pattern.shape(), (length, length));
        assert_eq!(pattern.num_nonzero(), 2 * length);
        for i in 0..length {
            assert!(pattern.index(i, i).is_some());
            assert!(pattern.index(i, (i + 1) % length).is_some());
            assert!(pattern.index(i, (i + 2) % length).is_none());
        }

        for &j in pattern.nonzeros(0) {
            assert!(j == 0 || j == 1);
        }
    }

    #[test]
    #[should_panic]
    fn test_sparsity_pattern_out_of_bounds() {
        let length = 16;
        let mut nonzeros = vec![];
        for i in 0..length {
            nonzeros.push((i, i));
            nonzeros.push((i, i + 1));
        }

        let _pattern =
            SparsityPattern::new_from_entry_list((length, length), &nonzeros);
    }
}
